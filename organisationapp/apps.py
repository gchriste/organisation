from django.apps import AppConfig


class OrganisationappConfig(AppConfig):
    name = 'organisationapp'
